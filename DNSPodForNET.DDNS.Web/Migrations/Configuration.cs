namespace DNSPodForNET.DDNS.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DNSPodForNET.DDNS.Web.Models.DDNSDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DNSPodForNET.DDNS.Web.Models.DDNSDbContext context)
        {

        }
    }
}

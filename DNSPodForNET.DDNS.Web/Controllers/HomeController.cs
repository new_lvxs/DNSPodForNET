﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DNSPodForNET.DDNS.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DDNS()
        {
            var recordName = Request["record"] ?? "";
            var domainName = Request["domain"] ?? "";
            var token = Request["token"] ?? "";
            var ipAddress = Request["ip"] ?? Request.UserHostAddress;

#if DEBUG
            //本地调试使用
            recordName = "Test";
            domainName = "zwsdk.cn";
            ipAddress = "1.1.1.1";
#endif
            DnsPodRecord dpr = new DnsPodRecord(token);
            var recordList = dpr.List(new { sub_domain = recordName, domain = domainName });
            int recordId = 0;
            if (recordList.records != null && recordList.records.Count > 0)
            {
                recordId = recordList.records[0].id;
            }
            else
            {
                recordId = dpr.Create(domainName, recordName, "192.168.1.1");
            }
            var flag = false;
            if (recordId > 0)
            {
                flag = dpr.Ddns(domainName, recordId, ipAddress, recordName);
            }
            return Content(flag.ToString());
        }
    }
}
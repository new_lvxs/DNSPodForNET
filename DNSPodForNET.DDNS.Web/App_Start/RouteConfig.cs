﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
/****************************************************
◇作者：郑伟
◇说明：RouteConfig
◇版本号：V1.0
◇创建日期：2017/7/21 11:20:26
◇操作记录：
*****************************************************/
namespace DNSPodForNET.DDNS.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "DDNS",
                url: "ddns/{id}",
                defaults: new { controller = "Home", action = "DDNS", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
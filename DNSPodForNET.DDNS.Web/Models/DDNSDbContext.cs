﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
/****************************************************
◇作者：郑伟
◇说明：DbContext
◇版本号：V1.0
◇创建日期：2017/7/21 11:33:20
◇操作记录：
*****************************************************/
namespace DNSPodForNET.DDNS.Web.Models
{
    public class DDNSDbContext : DbContext
    {
        public DDNSDbContext() : base("DefaultConnection")
        {

        }

        public DbSet<DDNSInfo> DdnsInfo { get; set; }
    }
}
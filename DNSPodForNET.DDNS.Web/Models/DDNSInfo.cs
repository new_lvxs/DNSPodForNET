﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
/****************************************************
◇作者：郑伟
◇说明：DDNSInfo
◇版本号：V1.0
◇创建日期：2017/7/21 11:35:36
◇操作记录：
*****************************************************/
namespace DNSPodForNET.DDNS.Web.Models
{
    public class DDNSInfo
    {
        public Guid ID { get; set; }

        [MaxLength(50)]
        public string DNSPodTokenId { get; set; }

        /// <summary>
        /// 域名
        /// </summary>
        [MaxLength(200)]
        [Index]
        public string DomainName { get; set; }

        /// <summary>
        /// 记录名
        /// </summary>
        [MaxLength(50)]
        [Index(IsUnique = true)]
        public string RecordName { get; set; }

        /// <summary>
        /// 当前IP
        /// </summary>
        [MaxLength(200)]
        public string CurrentIpAddress { get; set; }

    }
}
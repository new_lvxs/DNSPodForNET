# DNSPod C# 类库项目

## 项目说明
- 使用DNSPod官方API接口
- 基于.NET Framework 4.5
- 使用C# 4.0中特性dynamic
- 使用JSON.NET
- 基础信息可配置
- DDNS Web Site [http://ddns.zwsdk.cn](http://ddns.zwsdk.cn/)

## 更新日志

#### 2017-08-30
- 添加 DDNS 站点请求参数说明

#### 2016-8-17
- 记录操作增加对domain的支持
- 修改项目名称及命名空间
- 测试项目中使用domain操作

#### 2015-8-23
- 修改fraemwework版本，从4.0改为4.5，原4.0版本，请查看DNSPod_HttpWebRequest分支

- 修改对网络资源的请求方式，原：HttpWebRequest，现：HttpClient

#### 2015-7-25：
1. 解决bug：解决原先存在的问题，使用UserLog接口时，返回的log中数据为字符串数组，无法正常获取问题

2. 删除取消原有的自定义json字符串解析类

3. 使用Json.NET中的LINQ to JSON（可参考官方说明：[LINQ to JSON](http://www.newtonsoft.com/json/help/html/LINQtoJSON.htm)）

如何配置，请参阅DNSPodUnitTest文件夹中的App.config文件。

### 联系方式

- email:zhengwei@zwsdk.cn

- DnsPod官方QQ群:150138660

﻿/********************************************************
 * Author:developer.zheng
 * CreateTime:2015/9/10 00:27:35
 * Description:
 * 
 * Update History:
 * 
 ********************************************************/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNSPodForNET;
using System.Collections.Generic;
using System.Threading;

namespace DNSPodForNETUnitTest
{
    /// <summary>
    /// 对批量操作接口进行测试
    /// </summary>
    [Ignore]
    public class UnitTestDnsPodBatch
    {
        static List<string> domainIds;
        DNSPodBatch api = new DNSPodBatch();
        static int jobId = 0;
        [TestMethod]
        public void TestDomainCreate()
        {
            var result = api.DomainCreate(new string[] { "testdomaincreate.com", "testbatchdomaincreate.com" });
            if ("1".Equals(result.status.code.Value))
            {
                jobId = Convert.ToInt32(result.job_id);
                Console.WriteLine(result.job_id.ToString());
            }
            Console.WriteLine(result.status.message);
            Assert.AreEqual("1", result.status.code.Value);
        }

        [TestMethod]
        public void TestGetBatchDetail()
        {
            //批量添加域名是异步操作，需要等待DNSPod后台操作完成，具体操作时间视DNSPod后台情况而定
            //故此处等待时间可设置长点，以确保所有任务完成，以测试完整流程
            Thread.Sleep(5000);
            if (jobId > 0)
            {
                var jobStatus = api.Detail(jobId);
                Console.WriteLine(jobStatus.status.code.ToString());
                Console.WriteLine(jobStatus);
                if (null != jobStatus.detail)
                {
                    domainIds = new List<string>();
                    foreach (var item in jobStatus.detail)
                    {
                        if ("ok".Equals(item.status.Value))
                        {
                            domainIds.Add(item.domain_id.Value);
                        }
                    }
                }
            }
        }

        [TestMethod]
        public void TestRecordCreate()
        {
            var result = api.RecordCreate(domainIds.ToArray(),
                new List<object>()
                {
                    new {
                        sub_domain = "www,wap,bbs",
                        record_type = "A",
                        record_line = "默认",
                        value = "11.22.33.44",
                        ttl = 600
                    }
                }
            );
            Assert.AreEqual("1", result.status.code.Value);
        }

    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNSPodForNET;

namespace DNSPodForNETUnitTest
{
    [TestClass]
    public class UnitTestDnsPodDomainRecord
    {
        static int _domainId = 0, _recordId = 0;
        //默认不对主机记录和IP地址进行更改，测试API能否通过
        string subDomain = "ttt", ip = "192.168.199.10", domainName = "testdnspodapicreate.cn";

        [TestMethod]
        public void CreateDomain()
        {
            var api = new DnsPodDomain();
            dynamic result = api.Create(new { domain = domainName });
            Assert.AreNotEqual(0, result);
        }

        [TestMethod]
        public void GetDomainId()
        {
            var api = new DnsPodDomain();
            dynamic result = api.List(new { keyword = "" });//此处填写您域名的名称,为确保准确定位,请填写完整的域名,如:baidu.com
            var domains = result.domains;
            var domain = domains[0];
            _domainId = Convert.ToInt32(domain.id);
            Assert.AreNotEqual(0, _domainId);
        }

        [TestMethod]
        public void CreateRecord()
        {
            var record = new DnsPodRecord();
            _recordId = record.Create(domainName, subDomain, ip);//使用文档最新支持，域名，记录名，IP
            Assert.AreEqual(true, _recordId > 0);
        }

        [TestMethod]
        public void ModifyRecord()
        {
            var record = new DnsPodRecord();
            var result = record.Modify(domainName, _recordId, ip, subDomain);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void Ddns()
        {
            var record = new DnsPodRecord();
            var result = record.Ddns(domainName, _recordId, ip);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void RemoveRecord()
        {
            var record = new DnsPodRecord();
            var result = record.Remove(domainName, _recordId);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void RemoveDomain()
        {
            var api = new DnsPodDomain();
            var flag = api.Remove(domainName);
            Assert.AreEqual(true, flag);
        }

    }
}

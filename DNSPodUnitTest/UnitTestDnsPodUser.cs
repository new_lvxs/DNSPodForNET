﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DNSPodForNET;

namespace DNSPodForNETUnitTest
{
    [Ignore]
    public class UnitTestDnsPodUser
    {

        [TestMethod]
        public void GetVersion()
        {
            var result = new DnsPodUser().Version();
            string version = result.status.message;
            Assert.AreEqual("4.6", version);
        }

        [TestMethod]
        public void UserInfo()
        {
            var user = new DnsPodUser().UserDetail();
            string realname = user.info.user.real_name;
            Assert.AreEqual("", realname);
        }

        [Ignore]
        public void ModifyUserPassword()
        {
            var flag = new DnsPodUser().ModifyUserPassword("your old password here", "new password");
            Assert.AreEqual(true, flag);
        }

        [TestMethod]
        public void GetUserLog()
        {
            var api = new DnsPodUser();
            //返回信息
            var result = api.UserLog();
            //判断是否正常调用
            Assert.AreEqual(1, (int)result.status.code);
            //取得log对象
            var logs = result.log;
            //获得第一条记录
            var firstLog = logs[0];
            Console.WriteLine(firstLog);
            Assert.AreNotEqual("", firstLog);
        }
    }
}
